# Lab 4: Introduction to Ansible Tower

<!-- TOC -->

- [Lab 4: Introduction to Ansible Tower](#lab-4-introduction-to-ansible-tower)
  - [About this Lab](#about-this-lab)
  - [Why Ansible Tower?](#why-ansible-tower)
  - [Architecture and Installation](#architecture-and-installation)
  - [Ansible Tower Concepts](#ansible-tower-concepts)
    - [Dashboard](#dashboard)
    - [Projects](#projects)
    - [Inventories](#inventories)
    - [Credentials](#credentials)
    - [Templates](#templates)
    - [Jobs](#jobs)
  - [Create an Inventory](#create-an-inventory)
  - [Machine Credentials](#machine-credentials)
  - [Run Ad Hoc Commands](#run-ad-hoc-commands)
  - [Using Variables](#using-variables)
    - [Setup Git Repository](#setup-git-repository)
  - [Create a Job Template and Run a Job](#create-a-job-template-and-run-a-job)
  - [Ansible Tower Role Based Access Control](#ansible-tower-role-based-access-control)
    - [Ansible Tower Users](#ansible-tower-users)
    - [Ansible Tower Teams](#ansible-tower-teams)
    - [Granting Permissions](#granting-permissions)
    - [Test Permissions](#test-permissions)

<!-- /TOC -->

## About this Lab

This lab is about giving an overview and providing hands-on experience
with Ansible Tower. It’s assumed that attendees already have some basic
Ansible knowledge (concepts, Playbook writing etc) as the labs are
covering Tower-specific topics.

## Connecting to Ansible Tower
Point your web browser to https://ansible-tower-</GUID/>.rhpds.opentlc.com or click on your Lab GUID page that is open in your browser 
and open the link for your Ansible Tower Where <GUID> is your unique identifier as described in Lab 0.
login as user “admin” with the password “r3dh4t1!” as mentioned previously in this guide.


## Why Ansible Tower?


Ansible Tower is a web-based UI that provides an enterprise solution for
IT automation. Ansible Tower...

-   has a user-friendly dashboard
-   complements Ansible, adding automation, visual management, auditing, and monitoring capabilities.
-   provides role based access controls to administrators.
-   graphically manages or synchronizes inventories with a wide variety of sources.
-   a feature-rich RESTful API
-   And much more...

## Architecture and Installation

Ansible Tower is a Django web application that requires Ansible to be
installed. It also relies on a database back-end that uses PostgreSQL.
The default installation installs all components (web application, REST
API, and database) required by Ansible Tower on a single machine.


## Ansible Tower Concepts

To start using Ansible Tower, there are some concepts and naming conventions to be aware of.

### Dashboard

When logged in to Ansible Tower using the web UI, the administrator can
view a graph that shows

-   recent job activity
-   the number of managed hosts
-   quick links to lists of hosts with problems.

The dashboard also displays real time data about the execution of tasks
completed in playbooks.

### Projects

![menu1](images/image93.png)

Projects are logical collections of Ansible playbooks in Ansible Tower.
These playbooks either reside on the Ansible Tower instance, or in a
source code version control system supported by Tower (git, for example).

### Inventories

![menu2](images/image18.png)

An Inventory is a collection of hosts against which jobs may be
launched, the same as an Ansible inventory file. Inventories are divided
into groups and these groups contain the actual hosts. Groups may be
populated manually, by entering host names into Tower, or from one of
Ansible Tower’s supported cloud providers.

### Credentials

![menu3](images/image58.png)

Credentials are utilized by Tower for authentication when launching Jobs
against machines, synchronizing with inventory sources, and importing
project content from a version control system. Credential configuration
can be found in the Settings.

Tower credentials are imported and stored encrypted in Tower, and are
not retrievable in plain text on the command line by any user. You can
grant users and teams the ability to use these credentials, without
actually exposing the credential to the user.

### Templates

![menu4](images/image3.png)

A job template is a definition and set of parameters for running an
Ansible job. Job templates are useful to execute the same job many
times. Job templates also encourage the reuse of Ansible playbook
content and collaboration between teams. To execute a job, Tower
requires that you first create a job template.

### Jobs

![menu5](images/image31.png)

A job is basically an instance of Tower launching an Ansible playbook
against an inventory of hosts.

## Create an Inventory

The first thing we need is an inventory of your managed hosts. This is
the equivalent of an inventory file in Ansible Engine.

For your convenience, all the inventories we will use today are already
created for you, but you can add a dynamic inventory source from many
different sources, such as Satellite 6 as we have already added in a previous
lab. This creates a powerful way to manage systems with Ansible Tower
that you’re also managing with Satellite.

## Machine Credentials

One of the great features of Ansible Tower is to make credentials usable
to users without making them visible. To allow Tower to execute jobs on
remote hosts, you must configure connection credentials.

As mentioned, for your convenience we have already created these for
you.

## Run Ad Hoc Commands


As you’ve probably done with Ansible before, you can run ad hoc commands
from Tower as well.

-   In the web UI go to INVENTORIES → Example.com Clients
-   Click the SOURCES button to change to the sources view, then click the "Start sync process" button to refresh the list of hosts

    * Note: This would typically already be updated for you, but due to the short duration of the lab we are foring a refresh to make sure all hosts are listed.

-   Click the HOSTS button to change into the hosts view and select the
    four hosts that start with "ic" by ticking the boxes to the left of the host entries.
-   Click RUN COMMANDS. In the next screen you have to specify the
    command:

-   As MODULE choose Ping
-   For MACHINE CREDENTIAL click the magnifying glass icon and select
    Example.com SSH Password.
-   Click LAUNCH, sit back and enjoy the show…​ when completed you
    should see SUCCESS with a pong response like below.

![](images/image37.png)

![](images/image49.png)

![](images/image90.png)

Tip: The simple Ping module doesn’t need options. For the command module
you need to supply the command to run as an argument.

Try other modules in ad hoc commands, as well:

Find the userid of the executing user using an ad hoc command.
-   MODULE: command
-   ARGUMENTS: id
-   MACHINE CREDENTIAL: Example.com SSH Password

Print out /etc/shadow.
-   MODULE: command
-   ARGUMENTS: cat /etc/shadow
-   MACHINE CREDENTIAL: Example.com SSH Password


-   Re-run the last ad hoc command but this time tick the ENABLE
    PRIVILEGE ESCALATION box.

Tip: For tasks that have to run as root you need to escalate the
privileges. This is the same as the become: yes you’ve probably used
often in your Ansible Playbooks.

## Add a new Project

A Tower Project is a logical collection of Ansible playbooks. You can
manage playbooks by either placing them manually on your Tower server,
or by placing your playbooks into a source code management (SCM) system
supported by Tower, including Git, Subversion, and Mercurial.

You should definitely keep your Playbooks under version control. In this
lab we’ll use Playbooks kept in a Git repository.

### Setup Git Repository

For this lab you will use a pre-configured Git repository on
tower.example.com that can be accessed via SSH. A Playbook to set the
Message Of The Day(motd) has already been committed to the repository:

```
$ cat motd.yml

---

- hosts: all
  tasks:
        - template:
            src: ./motd.j2
            dest: /etc/motd

$ cat motd.j2

{{ motd }}

```

Create the Project

-   In the PROJECTS view click +ADD
-   NAME: Easy MOTD
-   ORGANIZATION: Default
-   SCM TYPE: Git
-   Point to the Git repo on github
-   SCM URL: https://github.com/IPvSean/easy_motd
-   SCM CREDENTIAL: \<leave blank\>
-   SCM UPDATE OPTIONS: Tick all three boxes to always get a fresh copy
    of the repository and to update the repository when launching a job.
-   Click SAVE

![](images/image26.png)

Sync the Project again with the Git repository by going to the
PROJECTS view and clicking the cloudy Start an SCM Update icon to the
right of the Project.

![](images/image94.png)

After starting the sync job, go to the JOBS view, find your job and
have a look at the details. You should see successful output where
there was a change on the host, that is the playbook being
downloaded to Tower.

## Create a Job Template and Run a Job


A job template is a definition and set of parameters for running an
Ansible job. Job templates are useful to execute the same job many
times. So before running an Ansible Job from Tower you must create a Job
Template that pulls together:

-   Inventory: On what hosts should the job run?
-   Credentials for the hosts
-   Project: Where is the Playbook?
-   What Playbook to use?

Okay, let’s just do that:

-   Go to the TEMPLATES view and click +ADD → JOB TEMPLATE

-   NAME: MOTD
-   JOB TYPE: Run
-   INVENTORY: Example.com Clients
-   PROJECT: Easy MOTD
-   PLAYBOOK: motd.yml
-   CREDENTIAL: Example.com SSH Password
-   We need to run the tasks as root so check Enable privilege
    escalation
-   This is the fun part, where we get to define the MOTD variable!

-   If the LIMIT field is blank this will change the MOTD on all clients
    in the Example.com Clients inventory. To change only one or a few
    systems you can specify them in the LIMIT field like in the
    screenshot.
-   In the VARIABLES box for the template provide the value

`motd: This is my MOTD from the Management Lab, Summit 2019!`

![](images/image27.png)

-   Click SAVE

Start a Job using this Job Template by going to the TEMPLATES view and
clicking the rocket icon. Have a good look at the information the view
provides.

![](images/image019.png)

Tip: This might take some time because you configured the Project to
update the SCM on launch.

After the Job has finished go to the JOBS view:

-   All jobs are listed here, you should see directly before the
    Playbook run an SCM update was started.
-   This is the Git update we configured for the PROJECT on launch!

## Ansible Tower Role Based Access Control

You have already learned how Tower separates credentials from users.
Another advantage of Ansible Tower is the user and group rights
management.

### Ansible Tower Users

There are three types of Tower Users:

-   Normal User: Have read and write access limited to the inventory and
    projects for which that user has been granted the appropriate roles
    and privileges.
-   System Auditor: Auditors implicitly inherit the read-only capability
    for all objects within the Tower environment.
-   System Administrator: Has admin, read, and write privileges over the
    entire Tower installation.

Let’s create a user:

In the Ansible Tower menu under ACCESS there's a "Users" link, click it, please.

![](images/image99.png)

Click the +ADD icon to create a new user.

Fill in the values for the new user:

-   FIRST NAME: Ansible
-   LAST NAME: Rocks
-   ORGANIZATION: Default
-   EMAIL: arocks@example.com
-   USERNAME: arocks
-   PASSWORD: P@ssword1!
-   Confirm password
-   USER TYPE: Normal User

-   Click SAVE

![](images/image6.png)

### Ansible Tower Teams

A Team is a subdivision of an organization with associated users,
projects, credentials, and permissions. Teams provide a means to
implement role-based access control schemes and delegate
responsibilities across organizations. For instance, permissions may be
granted to a whole Team rather than each user on the Team.

Create a Team:

In the Ansible Tower menu under ACCESS there's a "Teams" link, click it, please.

![](images/image98.png)

-   Click +ADD and create a team named Web Content.
-   Select ORGANIZATION: Default
-   Click SAVE

![](images/image97.png)

Now you can add a user to the Team:

-   Switch to the Users view of the Web Content Team by clicking the
    USERS button.
-   Click +ADD and select the arocks user, you may need to click to Page 2
    in the user list or use SEARCH.
-   The dialog now asks for a role to assign, the following permission
    settings are available:  

-   Admin: This User should have privileges to manage all aspects of the
    team
-   Member: This User should be a member of the team
-   Read: This User can view the team

-   Assign the Member role

-   Click SAVE
-   You may receive a 404 error due to system load, if this happens,
    ignore the error, simply refresh the page, and click Users to verify
    arocks is listed in the Web Content group as a Member.

![](images/image96.png)

Now click the PERMISSIONS button in the TEAMS view, you will be greeted
with "No Permissions Have Been Granted".

![](images/image81.png)

Permissions allow to read, modify, and administer projects, inventories,
and other Tower elements. Permissions can be set for different
resources.

### Granting Permissions

To allow users or teams to actually do something, you have to set
permissions. The user arocks should only be allowed to modify content of
the assigned webservers.

-   In the Permissions view of the Team Web Content click the + GRANT
    PERMISSIONS button.
-   A new window opens. You can choose to set permissions for a number
    of resources.

-   Select the resource type JOB TEMPLATES
-   Choose the Insights Scan template  by ticking the box next to it.
    You may need to click to the a different page in the selector until
    you get the Insights Scan template.

-   The second part of the window opens, here you assign roles to the
    selected resource.

-   Choose EXECUTE

-   Click SAVE

### Test Permissions

Now log out of Tower’s web UI and in again as the arocks user.

-   Go to the TEMPLATES view, you should notice for Ansible Rocks only the
    Insights Scan template is listed. He is allowed to view and lauch,
    but not to edit the Template.
-   Launch the Job Template. Click the rocket icon to the right of the
    job template in the Templates list (at bottom of screen, or from
    main Templates view)
-   In the following JOBS view have a good look around, note in the play
    recap that the scan should have completed successfully, with each
    host reporting ok from the playbook output.

![](images/image95.png)

Check the result in the output window that appears:

Just recall what you have just done: You enabled a restricted user to
run an Ansible Playbook

-   Without having access to the credentials
-   Without being able to change the Playbook itself
-   But with the ability to change variables you predefined!
-   This user can do NOTHING but launch the playbook we specified.
    Cannot see projects or inventories (unless you allow them to), you
    have granular control over access to these users.

**Tip** This capability is one of the main points of **Red Hat Ansible Tower**!

Continue to the next step: [Lab 5: Automatic Remediation with Red Hat Insights and Ansible Tower](../lab5-insights-and-tower/index.md)
